/*
    CICD AppRole - GitLab Runner Role
    Used to authenticate CICD Runners providing them with a list of policies to be able to dispatch
    executor jobs
*/
resource "vault_approle_auth_backend_role" "cicd_gitlab_runner" {
  backend = vault_auth_backend.cicd.path
  role_name = "cicd-gitlab-nomad-runner"

  token_no_default_policy = true
  token_ttl = "3600"
  token_max_ttl = "2592000"
  token_type = "service"
  token_policies = ["${vault_policy.cicd_gitlab_runner.name}"]
}

/*
    CICD AppRole - GitLab Runner Nomad Policy
    The Nomad policy for the GitLab CICD runner which should have permissions to dispatch jobs within
    the var.cicd_namespace namespace
*/
resource "nomad_acl_policy" "cicd_gitlab_runner" {
  name = "cicd-gitlab-nomad-runner"
  description = "Allows a GitLab CICD runner to dispatch an executor allocation and use nomad exec on it"

  rules_hcl = templatefile("policies/nomad/cicd-gitlab-nomad-runner.template.hcl", {
    namespace: var.cicd_namespace
  })
}

/*
    CICD AppRole - GitLab Runner Nomad Role
    The runner role for the Nomad secrets backend which allows generaton of a nomad token with the
    permissions to dispatch an Executor job
*/
resource "vault_nomad_secret_role" "cicd_gitlab_runner" {
  backend   = var.nomad_secret_backend_path
  role      = "cicd-gitlab-nomad-runner"
  type      = "client"
  policies  = ["${nomad_acl_policy.cicd_gitlab_runner.name}"]
}

/*
    CICD AppRole - GitLab Runner Vault Policy
    The Vault policy allowing the GitLab CICD Runner to obtain a Nomad token which will allow it to
    dispatch Executor Jobs
*/
resource "vault_policy" "cicd_gitlab_runner" {
  name = "cicd-gitlab-nomad-runner"

  policy = templatefile("policies/vault/cicd-gitlab-nomad-runner.template.hcl", {
    nomad_secret_backend_path: var.nomad_secret_backend_path
    nomad_credential_name: vault_nomad_secret_role.cicd_gitlab_runner.role
  })
}
