namespace "${namespace}" {
  capabilities = [
    "list-jobs",
    "read-job",
    "dispatch-job",
    "read-logs",
    "alloc-exec",
    "alloc-lifecycle"
  ]
}

node {
  policy = "read"
}

agent {
  policy = "read"
}
